# CCSDS Space Packet Implementation

An OS independent library implementation of the Space Packet protocol (CCSDS 133.0-B-1) , TC Space Data Link Protocol (CCSDS 232.0-B-3 ) and TM Space Data Link Protocol (CCSDS 132.0-B-2).


packet_assembly.c is responsible for assembly of space packet from the octets provided by the user.